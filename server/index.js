"use strict"

// Author : Manish Kumar Khedawat
/*jshint esversion: 6 */

const express = require('express');
//const path = require('path');
//const fs = require('fs');
const compression = require('compression');
//const FileStreamRotator = require('file-stream-rotator');
//const favicon = require('serve-favicon');
//const logger = require('morgan');
//var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
//const request = require('request');
//const uuidV1 = require('uuid/v1');

const app = express();
// Can be omitted, if Module ngx_http_gzip_module enabled in nginx
app.use(compression());

//'Access-Control-Allow-Origin' header
const cors = require('cors');
app.use(cors());

// Switch off the default 'X-Powered-By: Express' header
app.disable('x-powered-by');

// // logger Configs
// const logDirectory = path.join(__dirname, 'log');
// // ensure log directory exists
// fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
// // create a rotating write stream
// const accessLogStream = FileStreamRotator.getStream({
//     date_format: 'YYYYMMDD',
//     filename: path.join(logDirectory, 'access-%DATE%.log'),
//     frequency: 'daily',
//     verbose: false
// });
// app.use(logger('combined', {stream: accessLogStream}));

//Favicon Serve
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

//app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.get('/commit', function (req, res) {
    console.log(req.body);
    res.send("Hello");
})


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (app.get('env') === 'development') {
    console.log("App is Running in development mode");
}

// production error handler
// no stack-traces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: {}
    });
});



app.listen(3000)
console.log("hello");