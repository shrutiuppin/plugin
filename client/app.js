$(document).ready(function () {

    // sendRequest("https://git.target.com/ManishKhedawat/acc-test-repo/commit/ddb6704bf7b1f29e962032a972c4f983a8a3ff86");
    setTimeout(hideAnimation, 5000);

    function hideAnimation() {
        $(".showbox").hide();
        $(".welcome").text("Hello").addClass("animated zoomIn");
        $(".button").show().addClass("animated zoomIn");
    }

    sendRequest();

    $(".one").click(function () {
        $(".welcome").hide();
        $(".container").hide();
        $(".wrap").addClass("animated zoomIn");
    });

    function sendRequest() {
        $.get("http://localhost:3000/commit", {
            repoName: "abc",
            owner: "shruti",
            branchName: "init",
            commitID: "1"
        });
    }

    $(".mat-input").focus(function () {
        $(this).parent().addClass("is-active is-completed");
    });

    $(".mat-input").focusout(function () {
        if ($(this).val() === "")
            $(this).parent().removeClass("is-completed");
        $(this).parent().removeClass("is-active");
    })
});